/*Chargement des pages*/
function removeLoader(){
    $('#loading').hide();
}
$(window).load(function() {
    setTimeout(removeLoader, 800);
});


/*écran de chargement*/
function removeChargement(){
    $('#chargement-loading-image').hide();
}
function addChargement(){
    $('#chargement-loading-image').show();
}
function removeChargementTitle(){
    $('.titre-chargement').hide();
}
function addChargementTitle(){
    $('.titre-chargement').show();
}
function finChargement(){
    $('#chargement').hide();
}
function debutLogin(){
    $('#login').show();
}
$(window).load(function() {
    setTimeout(addChargement, 1);
    setTimeout(removeChargement, 1500);
    setTimeout(addChargementTitle, 1501);
    setTimeout(removeChargementTitle, 3500);
    setTimeout(addChargement, 3501);
    setTimeout(removeChargement, 4500);
    setTimeout(finChargement, 4500);
    setTimeout(debutLogin, 4500);
});


// ************** Recherche **************

$(document).ready(function(){

    $( "#show-categorie" ).click(function() {
        $( "#tags" ).append('<a href="" class="bouton bleuc categories-recherche">Aventures</a>');
        $( "#tags" ).append('<a href="" class="bouton bleuc categories-recherche">énigmes</a>');
        $( "#tags" ).append('<a href="" class="bouton bleuc categories-recherche">Combats</a>');
        $( "#tags" ).append('<a href="" class="bouton bleuc categories-recherche">éducatifs</a>');
        $( "#show-categorie" ).remove();
    });

});


// ******** Menu - options ************ //

function openNav() {
    document.getElementById("mySidenav").style.width = "80%";
   
}
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    
}

function ajoutFlou(){
    $('.fond-flou').css({
        'filter' : 'blur(4px)'
    });
}

function removeFlou(){
    $('.fond-flou').css({
        'filter' : 'blur(0px)'
    });
}

$( "#ouvrir-menu" ).click(function() {
    setTimeout(ajoutFlou, 50);
});
$( "#fermer-menu" ).click(function() {
    setTimeout(removeFlou, 500);
});

